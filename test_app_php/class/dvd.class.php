<?php

include_once '../abstract/product.abstract.php';

class Dvd extends Product
{  
  public function measureUnitValue($productInfo)
  {
    return htmlspecialchars(strip_tags($productInfo->size));
  }

  public function measureUnitName() : String 
  {
    return "Size:";
  }

  public function measureUnitType() : String 
  {
    return "MB";
  }
}