<?php

class ProductType
{
  const DVD = 'DVD';
  const Book = 'Book';
  const Furniture = 'Furniture';

  public static function getConstants()
  {
    $oClass = new ReflectionClass(__CLASS__);
    return $oClass->getConstants();
  }
}