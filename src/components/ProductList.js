import React from 'react';
import { Link } from 'react-router-dom';
import CheckBoxInput from './CheckBoxInput';
import '../css/productlist.css';

class ProductList extends React.Component
{ 
  refsArray = [];

  setRefs = (ref) => 
  {
    if (ref !== null) 
    {
      this.refsArray.push(ref);
      const lengthOfRefsArray = this.refsArray.length;
      const lengthOfProducts = this.props.products.length;
      if(lengthOfRefsArray > lengthOfProducts)
      {
        this.refsArray.splice(0, (lengthOfRefsArray - lengthOfProducts));
      }
    }
  };

  deleteSelected = () =>
  {
    this.props.massDeleteProducts(this.refsArray);
  };

  render()
  {
    return(
      <div className="product-list container-fluid">
        <div className="header">
          <h1>Product List </h1>
          <div className="buttons-wrapper">  
            <Link 
              to={"/add-product"} 
              className="btn btn-light"
            >
              ADD
            </Link>
            <Link 
              to={"/"} 
              className="btn btn-danger"
              onClick={this.deleteSelected}
              id="delete-product-btn"
            >
              MASS DELETE
            </Link>
          </div>
        </div>
        <hr />
        <div className="products d-flex flex-wrap" >
          {
            
            this.props.products.map((product, index) => {
              return (
                <div className="col-sm-3 px-2 py-2" key={index}>
                  <div className="card text-center">
                    <CheckBoxInput ref={this.setRefs}/>
                    <p>{product.sku}</p>
                    <p>{product.name}</p>
                    <p>{product.price}{" "}{product.currency}</p>
                    <p>{product.unitName}{" "}{product.unit}{" "}{product.unitType}</p>                   
                    <br />
                  </div>
                </div>
              )
            })
          }
        </div>         
      </div> 
    );
  }
}

export default ProductList;