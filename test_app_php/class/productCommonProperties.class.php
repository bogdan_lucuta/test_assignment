<?php

class ProductCommonProperties
{
  const SKU = array(
    "options" => array(
      array(
        "id" => "sku",
        "label" => "SKU",
        "type" => "text",
        "required" => true,
        "pattern" => "^[A-Za-z0-9]{3,8}$"
      )  
    ),
    "errorMessage" => "Please, provide the data of indicated type"       
  );

  const Name = array(
    "options" => array(
      array(
        "id" => "name",
        "label" => "Name",
        "type" => "text",
        "required" => true,
        "pattern" => "^[A-Za-z]{1,24}$"
      )
    ),
    "errorMessage" => "Please, provide the data of indicated type"
  );

  const Price = array(
    "options" => array(
      array(
        "id" => "price",
        "label" => "Price",
        "type" => "text",
        "required" => true,
        "pattern" => "^[0-9][0-9]{0,2}(.{2})*(,+0)?$"
      )
    ),
    "errorMessage" => "Please, provide the data of indicated type"  
  );

  public static function getConstants()
  {
    $oClass = new ReflectionClass(__CLASS__);
    return $oClass->getConstants();
  }
}