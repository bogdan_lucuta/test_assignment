<?php

include_once '../abstract/product.abstract.php';

class Furniture extends Product 
{
  public function measureUnitValue($productInfo)
  {
    return htmlspecialchars(strip_tags($productInfo->height)) . "x" . htmlspecialchars(strip_tags($productInfo->width)) . "x" . htmlspecialchars(strip_tags($productInfo->length));
  }
  
  public function measureUnitName() : String 
  {
    return "Dimension:";
  }

  public function measureUnitType() : String 
  {
    return "";
  }
}
