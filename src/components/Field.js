import React from 'react';
import '../css/field.css';

class Field extends React.Component
{
  state = { focused: false };

  render()
  { 
    const { emptyFieldError, skuError, onChange, errorMessage, values } = this.props;
    return(
      <>
        {this.props.options.map( el => 
          <div className="row mb-3" key={el.id}> 
            <label htmlFor={el.id} className="col-sm-2 col-form-label">{el.label}</label>
            <div className="col-sm-10">
              <input className="form-control form-control-sm"
                id={el.id}
                onChange={onChange}
                onFocus={() => (el.id === "size" || el.id === "length" || el.id === "weight") && this.setState({ focused: true })}
                onBlur={() => this.setState({ focused: true })}
                focused={this.state.focused.toString()}
                pattern={el.pattern}
                type={el.type}
                required={el.required} 
              />
              <span>{errorMessage}</span>
              {el.id === "sku" && <span className="sku-error">{skuError}</span>}
              {values[el.id] === undefined && <span className="empty-field-error">{emptyFieldError}</span>}
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Field;