import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import ProductList from './components/ProductList';
import AddProduct from './components/AddProduct';
import axios from 'axios';

class App extends React.Component 
{ 
  state = { 
    values: [],
    products: [],
    productCommonProperties: "",
    productAttributes: "",
    selectOptions: "",
    formSubmitError: "",
    selectValue: "Type switcher",
    selectFocused: false,
    skuError: "",
    emptyFieldError: ""
  };

  componentDidMount() 
  {
    this.getProductsFromDatabase();
    this.getProductCommonProperties();
    this.getProductAttributes();
    this.getSelectOptions();
  };

  getProductsFromDatabase = async () => 
  {
    try
    {   
      const response = await axios.get("./test_app_php/products/read.php");
      this.setState({ products: response.data.body });
    }
    catch(err)
    {
      console.log(err);
    }
  };

  getProductCommonProperties = async () => 
  {
    try
    {
      const response = await axios.get("./test_app_php/products/productCommonProperties.php");
      this.setState({ productCommonProperties: response.data[0] });
    }
    catch(err)
    {
      console.log(err);
    }
  };

  getProductAttributes = async () => 
  {
    try
    {
      const response = await axios.get("./test_app_php/products/productAttributes.php");
      this.setState({ productAttributes: response.data[0] });
    }
    catch(err)
    {
      console.log(err);
    }
  };

  getSelectOptions = async () => 
  {
    try
    {
      const response = await axios.get("./test_app_php/products/productType.php");
      this.setState({ selectOptions: response.data[0] });
    }
    catch(err)
    {
      console.log(err);
    }
  };

  insertProductIntoDatabase = async (newProduct) => 
  {
    try
    {
      await axios.post("./test_app_php/products/create.php", newProduct);
      this.getProductsFromDatabase();
    }
    catch(err)
    {
      console.log(err);
    }
  };

  deleteProductsFromDatabase = async (skus) => 
  {
    try
    {
      await axios.get(`./test_app_php/products/delete.php?skus=${skus}`);
      this.getProductsFromDatabase();
    }
    catch(err)
    {
      console.log(err);
    }
  };
  
  handleFormFocus = () =>
  {
    this.setState({ formSubmitError: "", skuError: "", selectFocused: false, emptyFieldError: "" });
  };
  
  setSelectValue = (value) => 
  { 
    this.setState({ selectValue: value });
    if ( value !== "Type switcher")
    {
      const comValues = [];
      Object.keys(this.state.productCommonProperties).map(key => comValues.push(this.state.productCommonProperties[key].options[0].id));
      Object.keys(this.state.productAttributes).map(key => 
        this.state.productAttributes[key].id === value ? 
          this.state.productAttributes[key].options.map(el => comValues.push(el.id))
        :''
      ); 
      Object.keys(this.state.values).map(key => comValues.indexOf(key) === -1 ? this.removeObjectFromState(key) : "");
    }
  };

  removeObjectFromState = (objectKey) => {
    let reducedObject = {};
    Object.keys(this.state.values).map((key) => 
      key !== objectKey ? 
        reducedObject[key] = this.state.values[key] 
      : ""
    );
    this.setState({ values: reducedObject });
  };

  onInputChange = (e) => 
  {
    this.setState({ values: {...this.state.values, [e.target.id]:e.target.value }});
  };

  saveProduct = (e) => 
  {
    const { sku, name, price, size, height, width, length, weight } = this.state.values;
    const valuesLen = Object.keys(this.state.values).length;
    const comPropLen = Object.keys(this.state.productCommonProperties).length;

    let skuExists = false;
    this.state.products.map((product) => (product.sku === sku ? skuExists = true : ''));
    if (skuExists) 
    { 
      e.preventDefault();
      this.setState
      ({
        skuError: "This SKU is in use. Please insert another SKU", 
        formSubmitError: "Please, submit required data" 
      });
    }
    else if (this.state.selectValue === "Type switcher") 
    {
      e.preventDefault();
      this.setState
      ({ 
        formSubmitError: "Please, submit required data", 
        selectFocused: true, 
        emptyFieldError: "Please, submit required data" 
      });
    }
    else   
    {      
      const comAttrLen = Object.keys(this.state.productAttributes[this.state.selectValue].options).length;
      if (valuesLen < comPropLen + comAttrLen)
      {
        e.preventDefault();
        this.setState
        ({ 
          formSubmitError: "Please, submit required data", 
          selectFocused: true, 
          emptyFieldError: "Please, submit required data" 
        });
      }
      else
      {
        const newProduct =  {
          sku,
          name,
          price,
          size,
          height,
          weight,
          length,
          width,
          type : this.state.selectValue
        };
        this.insertProductIntoDatabase(newProduct);
        this.resetFormValues();
      }
    }
  };

  massDeleteProducts = (refsArray) => 
  {
    if(refsArray !== [])
    {
      const skus = [];
      refsArray.map((el,index) => el.checked ?
        skus.push(this.state.products[index].sku)
        : '');
      if (skus.length > 0) 
      {
        this.setState({ products: [] },
          () => this.deleteProductsFromDatabase(skus));
      }
    }
  };    

  resetFormValues = () => 
  {
    this.setState({ values: "", selectValue: "Type switcher" });
  };

  render() 
  {
    return(
      <BrowserRouter>
        <Route path="/add-product">
          <AddProduct 
            productCommonProperties={this.state.productCommonProperties}
            productAttributes={this.state.productAttributes}
            selectOptions={this.state.selectOptions}
            selectValue={this.state.selectValue}
            values={this.state.values}
            setSelectValue={this.setSelectValue}
            onChange={this.onInputChange}
            onSaveProduct={this.saveProduct}
            onCancel={this.resetFormValues}
            selectFocused={this.state.selectFocused}
            handleFormFocus={this.handleFormFocus}
            skuError={this.state.skuError}
            emptyFieldError={this.state.emptyFieldError}
            formSubmitError={this.state.formSubmitError}
          />
        </Route>
        <Route path="/" exact>
          <ProductList 
            products={this.state.products}
            massDeleteProducts={this.massDeleteProducts}
          />
        </Route>
      </BrowserRouter>
    );
  }
}

export default App;