<?php

include_once '../config/db.connection.php';
include_once '../abstract/product.abstract.php';
include_once '../class/productFactory.class.php';

$connection = getConnection();

$productInfo = json_decode(file_get_contents("php://input"));
$unit = htmlspecialchars(strip_tags($productInfo->type))::measureUnitValue($productInfo);
$product = ProductFactory::create(htmlspecialchars(strip_tags($productInfo->sku)), htmlspecialchars(strip_tags($productInfo->name)), htmlspecialchars(strip_tags($productInfo->price)), $unit, htmlspecialchars(strip_tags($productInfo->type)));

if($product !== null)
{
  $product->create($connection);
}
