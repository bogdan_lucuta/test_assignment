<?php

include_once '../config/db.connection.php';
include_once '../class/productFactory.class.php';

$connection = getConnection();

$objects = Product::read($connection);

echo json_encode($objects);
?>