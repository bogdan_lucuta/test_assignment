import React from 'react';

const CheckBoxInput = React.forwardRef((props, ref) => 
  <input 
    className="delete-checkbox mx-2 mt-2" 
    type="checkbox"
    ref={ref}
  />);

export default CheckBoxInput;