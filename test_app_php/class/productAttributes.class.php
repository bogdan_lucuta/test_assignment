<?php

class ProductAttributes 
{
  const DVD = array(
    "options" => array(
      array(
        "id" => "size",
        "label" => "Size (MB)",
        "type" => "text",
        "required" => true,
        "pattern" => "^[1-9][0-9]{0,10}$",
      )
    ),
    "id" => "DVD",
    "errorMessage" => "Please, provide the data of indicated type",
    "description" => "*Please provide size in MB"    
  );

  const Furniture = array(
    "options" => array(
      array(
        "id" => "height",
        "label" => "Height (CM)",
        "type" => "text",
        "required" => true,
        "pattern" => "^[1-9][0-9]{0,10}$"
      ),
      array(
        "id" => "width",
        "label" => "Width (CM)",
        "type" => "text",
        "required" => true,
        "pattern" => "^[1-9][0-9]{0,10}$"
      ),
      array(
        "id" => "length",
        "label" => "Length (CM)",
        "type" => "text",
        "required" => true,
        "pattern" => "^[1-9][0-9]{0,10}$"
      )
    ),
    "id" => "Furniture",
    "errorMessage" => "Please, provide the data of indicated type",
    "description" => "*Please provide dimensions in HxWxL format"
  );

  const Book = array(
    "options" => array(
      array(
        "id" => "weight",
        "label" => "Weight (KG)",
        "type" => "text",
        "required" => true,
        "pattern" => "^[1-9][0-9]{0,10}$"
      )
    ),
    "id" => "Book",
    "errorMessage" => "Please, provide the data of indicated type",
    "description" => "*Please provide weight in KG"
  );

  public static function getConstants()
  {
    $oClass = new ReflectionClass(__CLASS__);
    return $oClass->getConstants();
  }
}