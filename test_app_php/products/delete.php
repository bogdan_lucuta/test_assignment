<?php

include_once '../config/db.connection.php';
include_once '../abstract/product.abstract.php';
include_once '../class/productFactory.class.php';

$connection = getConnection();

$list_of_skus = htmlspecialchars(strip_tags($_GET['skus']));

$skus = explode(",", $list_of_skus);
foreach($skus as $sku)
{
  $obj = ProductFactory::find_product($sku, $connection);
  $obj->delete($connection);
}
?>