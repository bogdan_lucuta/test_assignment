<?php

abstract class Product 
{
  public $sku;
  public $name;
  public $price;
  public $measureUnit;


  abstract public function measureUnitName() : String;
  abstract public function measureUnitType() : String;
  abstract public function measureUnitValue($productInfo);
  
  public function __construct($sku, $name, $price, $unit, $type)
  {
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->measureUnit = $unit;
    $this->type = $type;
  }

  public function currency() : String 
  {
    return "$";
  } 

  public function create($connection)
  {    
    $query = "INSERT INTO 
      Products (sku, name, price, unit, type) 
      VALUES 
        ('" .     $this->get_sku() 
        . "','" . $this->get_name() 
        . "','" . $this->get_price() 
        . "','" . $this->get_measureUnit() 
        . "','" . $this->get_type() 
        . "')";

    $stmt = $connection->prepare($query);
    $stmt->execute();
    return $stmt;
  }

  public static function read($connection)
  {
    $query = "SELECT * FROM Products ORDER BY id ASC";
    $stmt = $connection->prepare($query);
    $stmt->execute();
    $count = $stmt->rowCount();
    $products = array();
    $products["body"] = array();
    $products["count"] = $count;

    if($count > 0) 
    {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
      {
        extract($row);
        $unit = ProductFactory::get_unit($row);
        $productObject = ProductFactory::create($sku, $name, $price, $unit, $type);
        $productInfo = array(
          "sku" => $productObject->get_sku(),
          "name" => $productObject->get_name(),
          "price" => $productObject->get_price(),
          "unit" => $productObject->get_measureUnit(),
          "currency" => $productObject->currency(),
          "unitName" => $productObject->measureUnitName(),
          "unitType" => $productObject->measureUnitType()
        );
        array_push($products["body"], $productInfo);
      }
    }
    return $products;
  }

  public function delete($connection) 
  {
    $query = "DELETE FROM Products WHERE sku = '".$this->get_sku()."'";
    $stmt = $connection->prepare($query);
    $stmt->execute();
    return $stmt;
  }

  public function set_sku($sku) 
  {
    $this->sku = $sku;
  }

  public function get_sku() 
  {
    return $this->sku;
  }

  public function set_name($name) 
  {
    $this->name = $name;
  }

  public function get_name() 
  {
    return $this->name;
  }

  public function set_price($price) 
  {
    $this->price = $price;
  }

  public function get_price() 
  {
    return $this->price;
  }

  public function set_measureUnit($measureUnit) 
  {
    $this->measureUnit = $measureUnit;
  }

  public function get_measureUnit() 
  {
    return $this->measureUnit;
  }

  public function set_type($type) 
  {
    $this->type = $type;
  }

  public function get_type() 
  {
    return $this->type;
  }
}
?>