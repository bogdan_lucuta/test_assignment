<?php
include_once 'dvd.class.php';
include_once 'book.class.php';
include_once 'furniture.class.php';
include_once 'productType.class.php';

class ProductFactory
{
  public static function get_unit($row)
  {
    return $row["unit"];
  }

  public static function create($sku, $name, $price, $unit, $type)
  {
    return new $type($sku, $name, $price, $unit, $type);
  }

  public static function find_product($sku, $connection)
  {  
    $query = "SELECT * FROM Products WHERE sku = '" . $sku . "'";
    $stmt = $connection->prepare($query);
    $stmt->execute();
    $count = $stmt->rowCount();
    if($count > 0) 
    {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
      {
        extract($row);
        $unit = ProductFactory::get_unit($row);
        $productObject = ProductFactory::create($sku, $name, $price, $unit, $type);
        return $productObject;
      }
    }
  }
}