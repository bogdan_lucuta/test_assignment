import React from 'react';
import Field from './Field';

class Select extends React.Component
{
  handleSelect = (e) => 
  {
    this.props.setSelectValue(e.target.value);
  }
  render()
  {
    return(
      <>
        <div className="row mb-3">
          <label className="col-sm-6 col-form-label" htmlFor="productType">Type switcher</label>
          <div className="col-sm-6">
            <select value={this.props.selectValue} 
              onChange={(e) => this.handleSelect(e)}
              focused={this.props.selectFocused.toString()}
              className="form-select form-select-sm"
              id="productType"
            >
              <option value="Type switcher">Type switcher</option>
              {Object.keys(this.props.selectOptions).map((key, value) => {return <option key={value} value={key}>{key}</option>})}
            </select>
          </div>
        </div>
        {Object.keys(this.props.productAttributes).map((key) =>  
          key === this.props.selectValue ? 
            <div id={this.props.productAttributes[key].id} key={key}>
              <Field
                {...this.props.productAttributes[key]}
                emptyFieldError={this.props.emptyFieldError}
                onChange={this.props.onChange}
                values={this.props.values}
              /> 
            </div>
            : "" 
          )
        }
      </>
    );
  }
}

export default Select;