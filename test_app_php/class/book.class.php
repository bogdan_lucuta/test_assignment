<?php

include_once '../abstract/product.abstract.php';

class Book extends Product 
{ 
  public function measureUnitValue($productInfo)
  {
    return htmlspecialchars(strip_tags($productInfo->weight));
  }
  
  public function measureUnitName() : String 
  {
    return "Weight:";
  }

  public function measureUnitType() : String 
  {
    return "KG";
  }
}