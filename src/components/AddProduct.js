import React from "react";
import { Link } from "react-router-dom";
import Field from "./Field";
import Select from "./Select";
import "../css/addproduct.css";

  class AddProduct extends React.Component 
  {
    handleSubmit = (event) => 
    {
      event.preventDefault();
    };

    handleFormFocus = () => 
    {
      this.props.handleFormFocus();
    };

    handleSelect = (e) => 
    {
      this.props.setSelectValue(e.target.value);
    };

    onCancelProductAdding = () => 
    {
      this.props.onCancel();
    };

  render() 
  {  
    return(
      <div className="add-product container-fluid">
        <form id="product_form" 
          onSubmit={this.handleSubmit} 
          onFocus={this.handleFormFocus} 
        >
          <div className="header">
            <h1>Add Product</h1>
            <div className="buttons-wrapper">
              <Link to={"/"}
                onClick={this.props.onSaveProduct}
                className="btn btn-success"
              >
                Save
              </Link>
              <Link to={"/"}
                onClick={this.onCancelProductAdding}
                className="btn btn-warning"
              >
                Cancel
              </Link>
            </div>
          </div>
          <hr />
          <div className="row justify-content-center">
            <div className="col-sm-6">
              {Object.keys(this.props.productCommonProperties).map((key) =>  
                <Field key={key} 
                  {...this.props.productCommonProperties[key]} 
                  emptyFieldError={this.props.emptyFieldError} 
                  skuError={this.props.skuError}
                  onChange={this.props.onChange}
                  values={this.props.values}
                />
                )
              }
              <Select 
                values={this.props.values}
                setSelectValue={this.props.setSelectValue}
                onChange={this.props.onChange}
                selectValue={this.props.selectValue}
                selectOptions={this.props.selectOptions}
                selectFocused={this.props.selectFocused}
                productAttributes={this.props.productAttributes}
                handleFormFocus={this.handleFormFocus}
                emptyFieldError={this.props.emptyFieldError}
              />
            </div>
          </div>
          <span className="general-error">{this.props.formSubmitError}</span>
        </form>
      </div>
    );
  }; 
}

export default AddProduct;